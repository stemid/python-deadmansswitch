from app import create_app, load_config

load_config()
app = create_app()

if __name__ == '__main__':
    print('Initializing local web app...')
    localapp = create_app(local=True)
    localapp.run(debug=True)
