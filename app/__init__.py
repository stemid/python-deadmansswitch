import os
from pathlib import Path
from flask import Flask
from dotenv import load_dotenv
from app import routes

APP_NAME = 'dms'
DT_FORMAT = '%Y-%m-%dT%H:%M:%S.%f%z'


def load_config():
    env_path = Path('.') / '.env'
    if 'APP_ENV' in os.environ:
        env_path = '{path}.{env}'.format(
            path=env_path,
            env=os.getenv('APP_ENV')
        )
    load_dotenv(
        dotenv_path=env_path
    )


def create_app(local=False):
    """
    Create flask app object instance
    """

    app = Flask(__name__, static_url_path='', template_folder="static/pages")
    register_blueprints(app)

    if local:
        register_admin_blueprints(app)
        pass

    return app


def register_blueprints(app):
    """
    Register Flask blueprints necessary in production deployment.
    """
    app.register_blueprint(routes.api.health_bp)
    app.register_blueprint(routes.api.report_bp)
    return None


def register_admin_blueprints(app):
    """Register administrative blueprints."""
    app.register_blueprint(routes.api.admin_api_bp)
    app.register_blueprint(
        routes.admin.admin_bp,
        url_prefix='/admin'
    )
    return None
