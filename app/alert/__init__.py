from app.alert.pushover import Pushover


class Alert(object):

    def __init__(self, **kw):
        self.alert = Pushover(**kw)


    def send_alert(self, message):
        self.alert.send_message(message, priority=1)
