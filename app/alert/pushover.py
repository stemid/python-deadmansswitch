import os
import requests


class Pushover(object):

    def __init__(self, token=None, user_key=None):
        self.token = token or os.getenv('PUSHOVER_API_TOKEN')
        self.user_key = user_key or os.getenv('PUSHOVER_USER_KEY')
        self.session = requests.Session()
        self.api_url = 'https://api.pushover.net/1/messages.json'


    def send_message(self, message, **kw):
        params = {
            'token': self.token,
            'user': self.user_key,
            'message': message
        }
        if kw.get('title'):
            params['title'] = kw.get('title')
        if kw.get('priority'):
            params['priority'] = kw.get('priority', 1)
        self.session.post(self.api_url, data=params)
