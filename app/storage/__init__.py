import os
from datetime import datetime, timezone, timedelta
from typing import BinaryIO, Callable, List, Union

import jwt
from app.storage import bucketstore


class Storage(object):

    def __init__(self):
        """
        Return some storage backend, for now only s3 bucket. In future also
        GCP CloudStorage.
        """

        self.key_prefix = None
        self.store = bucketstore.get(
            os.getenv('AWS_DEFAULT_BUCKET'),
            create=True
        )


    def list(self) -> List:
        """Provide an override method that strips key_prefix from keys"""
        return [
            k.replace(
                str(self.store.key_prefix),
                '',
                1
            ) for k in self.store.list()
        ]


class TokenStore(Storage):

    def __init__(self):
        """Init token store with Storage as back end"""
        super().__init__()
        self.store.key_prefix = 'tokens/'
        self.secret = os.getenv('APP_SECRET')


    def generate_secret_token(self, expiration_days=365, **kw):
        expiration = datetime.utcnow().replace(
            tzinfo=timezone.utc
        ) + timedelta(days=expiration_days)
        return jwt.encode(
            {
                'exp': expiration,
                **kw
            },
            self.secret,
            algorithm='HS256'
        )


    def decode_secret_token(self, token):
        return jwt.decode(
            token,
            self.secret,
            algorithms=['HS256']
        )


class ReportStore(Storage):

    def __init__(self):
        """Init report store"""
        super().__init__()
        self.store.key_prefix = 'reports/'

