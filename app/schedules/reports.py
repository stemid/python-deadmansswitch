import os
from datetime import datetime, timedelta, timezone
from app import DT_FORMAT, APP_NAME, load_config
from app.storage import ReportStore, TokenStore
from app.alert import Alert
from app.logger import AppLogger

load_config()


class ProcessReports(object):

    def __init__(self, **kw):
        """Init storage backend"""
        self.reports = ReportStore()
        self.tokens = TokenStore()
        self.log = AppLogger(APP_NAME).logger
        self.log.setLevel(os.getenv('LOG_LEVEL', 'INFO'))
        self.alert_format = os.getenv(
            'ALERT_FORMAT',
            'Alert is firing: {report_id}'
        )


    def run(self, event, **kw):
        """This is run by your scheduler, AWS Lambda for example"""
        if event:
            kw = event.get('kwargs')

        for report_id in self.reports.list():
            self._process_report(report_id, kw.get('period'))


    def _process_report(self, report_id, period):
        """Actual processing of each report"""

        report = self.reports.store.get(report_id)

        try:
            token_id = report['token_id']
            token = self.tokens.store.get(token_id)
        except Exception as e:
            self.log.error('Issue with token ID, deleting: {error}'.format(
                error=str(e)
            ))
            self._delete(report_id)
            return

        if token['period'] != period:
            self.log.info('Period mismatch, skipping')
            return

        last_update = datetime.strptime(
            report['last_update'],
            DT_FORMAT
        )
        current_time = datetime.utcnow().replace(
            tzinfo=timezone.utc
        )
        td = current_time - last_update
        minutes = int(td.seconds/60)

        # Sometimes the remote servers run out of sync from your scheduled
        # report scanner job. This was mostly a fix for local testing.
        if current_time < last_update:
            self.log.info((
                'No action: '
                'current_time:{current} < last_update{update}'
            ).format(
                current=current_time,
                update=last_update
            ))
            return

        last_action = report.get('last_action')
        threshold_minutes = token.get('threshold_minutes', 1)

        if not last_action and minutes > threshold_minutes:
            self._alert(report_id, report, token)
            return

        if not last_action and minutes <= threshold_minutes:
            self.log.info('No action: {report_id}'.format(
                report_id=report_id
            ))
            return

        last_action_dt = datetime.strptime(
            last_action,
            DT_FORMAT
        )
        td = current_time - last_action_dt
        minutes = int(td.seconds/60)
        action_count = report.get('action_count')
        max_action_count = report.get('max_action_count', 3)

        if action_count < max_action_count and minutes > threshold_minutes:
            self._alert(report_id, report, token)
            return

        if action_count >= max_action_count:
            self._delete(report_id)
            return

        self.log.info('Alert active: {report_id}'.format(
            report_id=report_id
        ))


    def _alert(self, report_id, report, token):
        """Send alert to pushover API"""
        ct = datetime.utcnow().replace(
            tzinfo=timezone.utc
        )
        report['last_action'] = ct.isoformat()
        report['action_count'] = int(report.get('action_count', 0)+1)
        self.reports.store.set(report_id, report)
        alert_format = token.get(
            'alert_format',
            self.alert_format
        )

        self.log.info('Sending alert: {report_id}'.format(
            report_id=report_id
        ))

        if 'pushover_userkey' in token and 'pushover_token' in token:
            alert = Alert(
                token=token.get('pushover_token'),
                user_key=token.get('pushover_userkey')
            )
        else:
            alert = Alert()

        alert.send_alert(alert_format.format(
            report_id=report_id
        ))


    def _delete(self, report_id, report=None):
        """Delete report for inactivity"""
        self.log.info('Deleting report: {report_id}'.format(
            report_id=report_id
        ))
        self.reports.store.delete(report_id)


def run_schedule(event, **kw):
    """This is run by Lambda/CloudWatch"""
    schedule = ProcessReports()
    schedule.run(event, **kw)


if __name__ == '__main__':
    run_schedule(None, period=os.getenv('SCHEDULE_PERIOD', '1m'))
