$(document).ready(function() {
  'use strict'
  feather.replace();

  var toastError = (message, title='Error!') => {
    $('#toastHeader').html(title);
    $('#toastHeader').addClass('text-danger');
    $('#toastBody').html(message)
    $('.toast').toast('show');
  }

  var startSpinnerButton = (el, message) => {
    el.prop('disabled', true);
    el.html(
      `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> ${message}`
    );
  }

  var stopSpinnerButton = (el, message) => {
    el.prop('disabled', false);
    el.html(message);
  }

  var tokensTable = $('#tokens').DataTable({
    'ajax': {
      'url': '/api/v1/admin/token',
      'dataSrc': 'tokens'
    },
    //'lengthChange': false,
    //'paging': false,
    'pageLength': 25,
    'order': [[2, 'desc']],
    'columns': [
      { 'data': 'token.name' },
      {
        'data': 'token_id',
        'render': function(data, type, row, meta) {
          if (type === 'display') {
            data = '<a href="/admin/token/'+data+'">'+data+'</a>';
          }
          return data
        },
        'searchable': false
      },
      { 'data': 'token.created', 'searchable': false },
    ]
  })

  var reportsTable = $('#reports').DataTable({
    'ajax': {
      'url': '/api/v1/admin/report',
      'dataSrc': 'reports'
    },
    //'lengthChange': false,
    //'paging': false,
    'pageLength': 25,
    'order': [[2, 'desc']],
    'columns': [
      { 'data': 'report_id', },
      {
        'data': 'report.token_id',
        'render': function(data, type, row, meta) {
          if (type === 'display') {
            data = `<a href="/admin/token/${data}">${data}</a>`;
          }
          return data
        },
        'searchable': false
      },
      { 'data': 'report.last_update', 'searchable': false },
    ]
  })

  setInterval(() => {
    reportsTable.ajax.reload();
  }, 5000)

  $('#createTokenForm').submit((e) => {
    e.preventDefault();

    startSpinnerButton($('#createToken'), 'Creating...');
    var tokenData = new Object();
    tokenData['name'] = $('#inputName').val();
    tokenData['period'] = $('#inputPeriod').val();
    tokenData['expiration_days'] = parseInt($('#inputValid').val());

    if ($('#inputPushoverUserkey').val().length > 0)
      tokenData['pushover_userkey'] = $('#inputPushoverUserkey').val();
    if ($('#inputPushoverApitoken').val().length > 0)
      tokenData['pushover_token'] = $('#inputPushoverApitoken').val();
    if ($('#inputAlertFormat').val().length > 0)
      tokenData['alert_format'] = $('#inputAlertFormat').val()

    $.ajax({
      type: 'PUT',
      url: '/api/v1/admin/token',
      data: JSON.stringify(tokenData),
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      success: (data) => {
        $('#tokenModal').modal('toggle');
        stopSpinnerButton($('#createToken'), 'Create token');
        tokensTable.ajax.reload();
      },
      error: (jqXHR, textStatus, errorThrown) => {
        console.log(jqXHR);
        stopSpinnerButton($('#createToken'), 'Create token');
      }
    })
  })

  $('#refreshTokens').on('click', (e) => {
    e.preventDefault();
    tokensTable.ajax.reload();
  })

  $('#deleteToken').on('click', (e) => {
    e.preventDefault();
    startSpinnerButton($('#deleteToken'), 'Deleting...');

    $.ajax({
      type: 'DELETE',
      url: '/api/v1/admin/token/'+$('#inputToken').val(),
      success: (data) => {
        window.location.href = '/admin/token';
        stopSpinnerButton($('#deleteToken'), 'Delete');
      },
      error: (jqXHR, textStatus, errorThrown) => {
        stopSpinnerButton($('#deleteToken'), 'Delete');
      }
    })
  })
})
