import os
import json
from datetime import datetime, timezone
from uuid import uuid4
from flask import (
    request,
    app,
    Blueprint,
    render_template,
    redirect,
    url_for
)
from flask.views import View
from app.storage import TokenStore

admin_bp = Blueprint(
    'admin',
    __name__,
    template_folder='templates',
    static_folder='static'
)


class AdminIndex(View):

    def dispatch_request(self):
        return redirect(url_for('admin.tokens_view'))


class AdminTokens(View):

    def dispatch_request(self):
        """
        View tokens
        """

        return render_template('tokens.html')


class AdminToken(View):

    def dispatch_request(self, token_id):
        """
        View token
        """
        tokens = TokenStore()
        if token_id not in tokens.store:
            return redirect(url_for('admin.tokens_view'))
        token_data = tokens.store.get(token_id)

        return render_template(
            'token.html',
            token_data=token_data,
            token_id=token_id
        )


class AdminClient(View):

    def dispatch_request(self):
        """View clients"""
        return render_template('client.html')


class AdminReports(View):

    def dispatch_request(self):
        """View reports"""
        return render_template('reports.html')


class AdminReport(View):

    def dispatch_request(self):
        """View report"""
        return render_template('report.html')


admin_bp.add_url_rule('/', view_func=AdminIndex.as_view('index_view'))
admin_bp.add_url_rule('/token', view_func=AdminTokens.as_view('tokens_view'))
admin_bp.add_url_rule('/token/<string:token_id>', view_func=AdminToken.as_view('token_view'))
admin_bp.add_url_rule('/client', view_func=AdminClient.as_view('client_view'))
admin_bp.add_url_rule('/report', view_func=AdminReports.as_view('reports_view'))
