import os
import json
from datetime import datetime, timezone
from uuid import uuid4
from flask import request, app, Blueprint, jsonify
from flask_restful import Resource, Api
from app.storage import TokenStore

admin_api_bp = Blueprint('admin_api', __name__)
api = Api(admin_api_bp, prefix='/api/v1/admin')


class AdminToken(Resource):

    def put(self):
        """
        Create new client Token
        """

        if not request.is_json:
            return {'error': 'Body must be JSON'}, 400

        req_data = request.get_json()
        try:
            current_time = datetime.utcnow().replace(
                tzinfo=timezone.utc
            ).isoformat()
            token_data = {
                'name': req_data['name'],
                'period': req_data['period'],
                'created': current_time,
            }
        except KeyError as e:
            return {
                'error': str(e),
                'message': 'Missing key in JSON body'
            }, 400
        except Exception as e:
            return {
                'error': str(e),
                'message': 'Undefined failure'
            }, 400

        if 'alert_format' in req_data:
            token_data['alert_format'] = req_data['alert_format']

        try:
            tokens = TokenStore()
        except Exception as e:
            return {
                'message': 'Failed to init TokenStore',
                'error': str(e)
            }, 500

        token_id = str(uuid4())
        expiration_days = req_data.get('expiration_days', 365)
        token_data['token_id'] = token_id
        token_data['expiration_days'] = expiration_days

        try:
            token_data['jwt_token'] = tokens.generate_secret_token(
                expiration_days=expiration_days,
                user_id=token_id
            ).decode('utf-8')
        except Exception as e:
            return {
                'error': str(e),
                'message': 'Failed to create token'
            }, 500

        if 'pushover_userkey' in req_data and 'pushover_token' in req_data:
            token_data['pushover_userkey'] = req_data['pushover_userkey']
            token_data['pushover_token'] = req_data['pushover_token']

        try:
            tokens.store.set(token_id, token_data)
        except Exception as e:
            return {
                'error': str(e)
            }, 500

        return {
            'message': 'Token created',
            'token': {
                'token_id': token_id,
                **token_data
            }
        }


    def get(self, token_id=None):
        """
        Request token by id
        """

        try:
            tokens = TokenStore()
        except Exception as e:
            return {
                'message': 'Undefined error',
                'error': str(e)
            }, 500

        if token_id:
            if token_id not in tokens.store:
                return {'error': 'Token does not exist'}, 404

            return {
                'token_id': token_id,
                'token': tokens.store.get(token_id)
            }
        else:
            tokens_data = []
            for token_id in tokens.list():
                tokens_data.append({
                    'token_id': token_id,
                    'token': tokens.store.get(token_id)
                })
            return {'tokens': tokens_data}


    def post(self, token_id):
        """
        Update token
        """

        try:
            tokens = TokenStore()
        except Exception as e:
            return {
                'message': 'Undefined error',
                'error': str(e)
            }, 500

        current_time = datetime.utcnow().replace(
            tzinfo=timezone.utc
        ).isoformat()

        if token_id not in tokens.store:
            return {'error': 'Token not found'}, 404

        token_data = tokens.store.get(token_id)
        token_data['last_update'] = current_time
        try:
            del token_data['action_count']
            del token_data['last_action']
        except KeyError:
            pass
        tokens.store.set(token_id, token_data)

        return {
            'status': 'OK',
            'token_id': token_id,
            'token': token_data
        }


    def delete(self, token_id):
        """
        Delete report
        """

        try:
            tokens = TokenStore()
        except Exception as e:
            return {
                'message': 'Undefined error',
                'error': str(e)
            }, 500

        if not token_id in tokens.store:
            return {'error': 'Token not found'}, 404

        tokens.store.delete(token_id)

        return {'message': 'Token deleted'}


api.add_resource(
    AdminToken,
    '/token',
    '/token/<string:token_id>'
)
