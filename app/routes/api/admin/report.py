import os
import json
from datetime import datetime, timezone
from uuid import uuid4
from flask import request, app, jsonify
from flask_restful import Resource
from app.storage import ReportStore


class AdminReport(Resource):

    def get(self, report_id=None):
        """
        Request report by id
        """

        try:
            reports = ReportStore()
        except Exception as e:
            return {
                'message': 'Failed to init ReportStore',
                'error': str(e)
            }, 500

        if report_id:
            if report_id not in reports.store:
                return {'error': 'Report does not exist'}, 404

            return {
                'report_id': report_id,
                'report': reports.store.get(report_id)
            }
        else:
            reports_data = []
            for report_id in reports.list():
                reports_data.append({
                    'report_id': report_id,
                    'report': reports.store.get(report_id)
                })
            return {'reports': reports_data}


    def post(self, report_id):
        """
        Update report
        """

        try:
            reports = ReportStore()
        except Exception as e:
            return {
                'message': 'Failed to init ReportStore',
                'error': str(e)
            }, 500

        current_time = datetime.utcnow().replace(
            tzinfo=timezone.utc
        ).isoformat()

        if report_id not in reports.store:
            return {'error': 'Report not found'}, 404

        report_data = reports.store.get(report_id)
        report_data['last_update'] = current_time

        try:
            del report_data['action_count']
            del report_data['last_action']
        except KeyError:
            pass

        reports.store.set(report_id, report_data)

        return {
            'message': 'Report updated',
            'report_id': report_id,
            'report': report_data
        }


    def delete(self, report_id):
        """
        Delete report
        """

        try:
            reports = ReportStore()
        except Exception as e:
            return {
                'message': 'Undefined error',
                'error': str(e)
            }, 500

        if not report_id in reports.store:
            return {'error': 'Report not found'}, 404

        reports.store.delete(report_id)

        return {'message': 'Report deleted'}

