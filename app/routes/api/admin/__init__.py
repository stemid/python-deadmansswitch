from flask import Blueprint
from flask_restful import Api
from .token import AdminToken
from .report import AdminReport

admin_api_bp = Blueprint('admin_api', __name__)

token_api = Api(admin_api_bp, prefix='/api/v1/admin')
token_api.add_resource(
    AdminToken,
    '/token',
    '/token/<string:token_id>'
)

report_api = Api(admin_api_bp, prefix='/api/v1/admin')
report_api.add_resource(
    AdminReport,
    '/report',
    '/report/<string:report_id>'
)
