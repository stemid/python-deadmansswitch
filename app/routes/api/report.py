import os
import json
from datetime import datetime, timezone
from flask import request, app, Blueprint, jsonify
from flask_restful import Resource, Api
from app.storage import ReportStore

report_bp = Blueprint('report', __name__)
api = Api(report_bp, prefix="/api/v1")


class ClientReport(Resource):

    def get(self, report_id):
        """
        Request report
        """

        try:
            reports = ReportStore()
        except Exception as e:
            return {
                'message': 'Failed to init ReportStore',
                'error': str(e)
            }, 500

        if report_id not in reports.store:
            return {'error': 'Report does not exist'}, 404

        return {
            'report_id': report_id,
            'report': reports.store.get(report_id)
        }


    def post(self):
        """
        Update report
        """

        try:
            reports = ReportStore()
        except Exception as e:
            return {
                'message': 'Failed to init ReportStore',
                'error': str(e)
            }, 500

        current_time = datetime.utcnow().replace(
            tzinfo=timezone.utc
        ).isoformat()

        report_id = request.environ.get(
            'REMOTE_ADDR',
            request.remote_addr
        )
        token_id = request.headers.get('token_id')

        if not token_id:
            return {
                'message': 'No token ID'
            }, 422

        report_data = {}
        if report_id in reports.store:
            report_data = reports.store.get(report_id)
        report_data['last_update'] = current_time
        report_data['token_id'] = token_id

        # If alert has triggered, delete its remnants
        try:
            del report_data['action_count']
            del report_data['last_action']
        except KeyError:
            pass

        try:
            reports.store.set(report_id, report_data)
        except Exception as e:
            return {
                'message': 'Failed to store report',
                'error': str(e)
            }, 500

        return {
            'message': 'Report received',
            'report_id': report_id,
            'report': report_data
        }


api.add_resource(ClientReport, '/report', '/report/<string:report_id>')
