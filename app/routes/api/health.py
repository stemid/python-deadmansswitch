from flask import request, app, Blueprint
from flask_restful import Resource, Api

health_bp = Blueprint('api', __name__)
api = Api(health_bp, prefix="/api/v1")

class Health(Resource):

    def get(self):
        """
        Return 200 code to indicate healthy app
        """

        return {'status': 'OK'}

api.add_resource(Health, '/health')
