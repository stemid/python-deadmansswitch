# Dead Man's Switch

This is a Python setup that can act like a Dead Man's Switch. Meaning that it expects a HTTP callback every X minutes, and as soon as it doesn't receive that callback it will fire an alert.

The idea is that this can be deployed both to a regular server and to at least two different cloud vendors. That way you'll always have a backup.

## Install document

This documents how to setup your own DMS in your own AWS account and for your own Prometheus.

### Create virtualenv and install dependencies

    $ python3 -m venv .venv
    $ source .venv/bin/activate
    $ pip install -r requirements.txt

### Deploy to AWS cloud

This step requires that you have awscli installed and configured, you should have your credentials setup in ``~/.aws/``. [See here for more info on that](https://docs.aws.amazon.com/polly/latest/dg/setup-aws-cli.html).

Here is an example ``zappa_settings.json`` file.

    {
        "dmsapi_dev": {
            "app_function": "run.app",
            "aws_region": "eu-north-1",
            "profile_name": "default",
            "project_name": "dms",
            "runtime": "python3.7",
            "s3_bucket": "zappa-<rename this>",
            "api_key_required": false,
            "slim_handler": false,
            "http_methods": [
              "GET",
              "POST"
            ],
            "events": [{
              "function": "app.schedules.reports.run_schedule",
              "expression": "rate(1 minute)",
              "kwargs": {
                "period": "1m"
              }
            }],
            "environment_variables": {
              "LOG_LEVEL": "WARN",
              "ALERT_FORMAT": "Dead man's switch has stopped firing: {report_id}",
              "APP_SECRET": "<change this to a good secret string for JWT tokens>"
            },
            "authorizer": {
              "function": "app.auth.aws.lambda_handler",
              "result_ttl": 120,
              "token_header": "Authorization",
              "validation_expression": "^Bearer [\\w\\-\\.]+$"
            },
            "context_header_mappings": {
              "token_id": "authorizer.token_id"
            }
        }
    }

Change the following settings to your own;

* ``s3_bucket`` - Use a valid bucket name that is not used by anyone else in your region.
* ``APP_SECRET`` - Create a unique secret string that will be used for API tokens.

### Configure local admin GUI

Create a file called ``.env`` with the following info.

    AWS_ACCESS_KEY_ID=secret
    AWS_SECRET_ACCESS_KEY=secret
    AWS_DEFAULT_REGION=eu-north-1
    AWS_DEFAULT_BUCKET=some-unique-bucket-name
    PUSHOVER_USER_KEY=secret
    PUSHOVER_API_TOKEN=secret
    APP_SECRET=secret

* ``AWS_ACCESS_KEY`` & ``AWS_SECRET_ACCESS_KEY`` are not required if you have your ``~/.aws/`` profile setup.
* ``AWS_DEFAULT_BUCKET`` is not the same bucket as in ``zappa_settings.json``, this bucket is for storing tokens and reports from clients. Like a cheap database alternative. So again you must pick a unique name in your region.
* ``PUSHOVER_*`` - These settings are for alerting you via [Pushover.net](https://pushover.net) API when the Dead Man's Switch triggers.
* ``APP_SECRET`` - This must match the ``APP_SECRET`` setting from your ``zappa_settings.json`` file. You will use this to create JWT API tokens locally, store them on S3 and then they'll be used on AWS to authenticate your clients.

### Start local admin GUI

    $ python run.py

This starts on localhost:5000 by default so you should be able to visit [http://localhost:5000/admin](http://localhost:5000/admin) in your browser.

Click on Tokens and create a new token for your Prometheus setup.

### Configure Prometheus

This requires an alert rule that is constantly firing, as soon as it stops firing the Switch is triggered.

Here is an example alert rule that fires non-stop.

     - alert: DeadMansSwitch
       annotations:
         description: This Dead Man's Switch fires constantly.
         summary: Alerting DeadMansSwitch
       expr: vector(1)
       labels:
         severity: none

### Configure Alertmanager

In Alertmanager routes you can setup a route for it to always trigger a web hook.

    routes:
      - match:
          alertname: DeadMansSwitch
        receiver: 'DeadMansSwitch'
        group_wait: 0s
        group_interval: 1m
        repeat_interval: 50s

And lastly you need a receiver that is referenced in this route.

    receivers:
      - name: 'DeadMansSwitch'
        webhook_configs:
          - url: 'https://<some.server>/api/v1/report/<client_id>'
            send_resolved: false
            http_config:
              bearer_token: '<secret API token from API Gateway or whatever frontend you might be using>'

### Ansible deploy

I'm using Ansible to deploy a working Dead Man's Switch config, you'll find the [playbooks here](https://gitlab.com/stemid-ansible/playbooks/ansible-prometheus.git).

## Roadmap

* GCP deploy
* Document regular server deploy
